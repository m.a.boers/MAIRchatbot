#First open all folders, then read through all the subfolders to see if there are json files in there.
#Read all folders in the data folder and then in all subfolders read the json files.
import os, json, random
import keras
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM
from keras.utils import to_categorical

"""
Create and combine utterences with "speech act" labels, only needed to run once for all data, then saved in speechactfile.txt
"""

if not os.path.isfile("speechactfile.txt"):
    print("No speechactfile found, loading data from data folder...")
    mainpath = "data\\"
    subfolders = os.listdir(mainpath)                                       #Every of these folders contains a dataset.
    speechactfile = open("speechactfile.txt","w")                           #Create a file for a set of utterences labled with speech acts

    for folder in subfolders:
        jsonfolders = os.listdir(mainpath + folder)
        for jsonfolder in jsonfolders:                                      #Every of these folders contains a conversation.
            path = mainpath + folder + "\\" + jsonfolder + "\\"             #Get the path to the current folder.
            
            labelfile = open(path + "label.json").read()                    #First load the label json file.
            labeljson = json.loads(labelfile)
           
            for userturn in labeljson["turns"]:                             #For each user turn load the classifier and the transcription.
                speechactfile.write(userturn["semantics"]["cam"][:userturn["semantics"]["cam"].index("(")]+" "+ userturn["transcription"]+"\n") 
                
    speechactfile.close()                                                   #Close data file
    print("Done loading data.")
else:
    print("Speechactfile found, skipping data loading.")

"""
Load the data file with all combinations and split it in a training and a test data set.
"""  
class Classifier:

    def __init__(self):
 
        print("Splitting data in training and test sets...")
                                                         
        speechactfile85 = open("speechactfile85.txt","w")                           #Create a file for a training set of utterences labled with speech acts
        speechactfile15 = open("speechactfile15.txt","w")                           #Create a file for a test set of utterences labled with speech acts

        with open("speechactfile.txt", "r") as data:
            for line in data:
                r = random.random()
                if r < 0.85:                                                        #Add 85% to the training set at random
                    speechactfile85.write(line)
                else:                                                               #Add the other 15% to the test set at random
                    speechactfile15.write(line)

        speechactfile85.close()
        speechactfile15.close()            
                    
        """
        Create vocabulary mapping file with "speech act" labels to start with (to be encoded 2 - 17)
        """

        print("Building vocabulary and encoding data...")

        self.vocabulary = {}                                                             #Make a dictionary to put the vocabulary mapping in

        for i, speechact in enumerate(["ack","affirm","bye","confirm","deny","hello","inform","negate","null","repeat","reqalts","reqmore","request","restart","thankyou"]):	
            self.vocabulary[speechact] = i + 2                                           #Add the speechacts to the mapping file to start encoding with, from "2"

        """
        Encode training set of utterences with labels (max of 10 utterences and pad with 1s) and build vocabulary on the go with training data
        """	
        self.utt_length = 10                                                             #Max length of the utterences
        cur_index = 17                                                              #Index after adding the speechacts to the vocabulary 

        train_data = []
        train_labels = []

        with open("speechactfile85.txt","r") as trainingfile:                       #Open training file with utterences and speech act labels to read and close it when finished
            for line in trainingfile:                                               #For each line in training file
                data = []
                for i, word in enumerate(line.split()):                             #For each word in this line, and count the lines with i
                    if i == 0:
                        train_labels.append(self.vocabulary[word] - 2)                   #Always add the category as label, these are always already known in the vocabulary
                        continue
                    if i > self.utt_length:                                              #Go up to utt_length + 1, because the first word was the label not an input word
                        break														#Stop reading this line when encountered a 11th utterence 
                    if word in self.vocabulary:
                        data.append(self.vocabulary[word])                               #If the word is in the vocabulary, simply add the value to the data
                    else:
                        data.append(cur_index)                                      #If the word is unknown, add it to the vocabulary and increase the index for the next word
                        self.vocabulary[word] = cur_index
                        cur_index += 1
                for j in range(i,self.utt_length):												
                    data.append(1)								                    #Continue to write "1"s as padding up to 10 utterences
                train_data.append(data)                                             #When done, add the new data array to the training data set
                
        """
        Now that we have made the vocabulary and loaded the training data, load the test data.
        """
                   
        test_data = []
        test_labels = []

        with open("speechactfile15.txt","r") as testfile:
            for line in testfile:
                data = []
                for i, word in enumerate(line.split()):                             #For each word in this line, and count the lines with i
                    if i == 0:
                        test_labels.append(self.vocabulary[word] - 2)                    #Always add the category as label, these are always already known in the vocabulary
                        continue
                    if i > self.utt_length:                                              #Go up to utt_length + 1, because the first word was the label not an input word
                        break														#Stop reading this line when encountered a 11th utterence 
                    if word in self.vocabulary:
                        data.append(self.vocabulary[word])
                    else:
                        data.append(0)                                              #If the word we are testing is not in the vocabulary, add a 0
                for j in range(i,self.utt_length):												
                    data.append(1)								                    #Continue to write "1"s as padding up to 10 utterences
                test_data.append(data)
                  
        train_cat = to_categorical(train_labels)                                    #Change the labels to categorial (list of 0 and 1 per output node instead of integer numbers)
        test_cat = to_categorical(test_labels)
                   
        """
        Now set up the LSTM model and train the algorithm. Test the algorithm afterwards as well.
        """

        print("Starting LSTM...")

        hidden_size = 250                                                                       #Number of hidden layers and number of values per word vector
        voc_size = cur_index                                                                    #Number of values in the vocabulary, equal to the current index

        self.model = Sequential()                                                                    #Start with a model, it has a embedding layer, LSTM layer, dense relu layer and output softmax layer at least, can add more dense relu layers
        self.model.add(Embedding(voc_size, hidden_size, input_length=self.utt_length, mask_zero=True))    #Embedding layer, maps the words in the vocabulary to a vector to keep word properties
        self.model.add(LSTM(hidden_size))                                                            #LSTM layer with a number of hidden layers, performs the actual LSTM
        self.model.add(Dense(voc_size, activation='relu'))                                           #Dense layer, can add more if needed, need testing to see what is best
        self.model.add(Dense(15, activation='softmax'))                                              #Output layer, picking the best category by taking the maximum (softmax) of the nodes

        self.model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])  #Make the model using categorical crossentropy (so 15 output nodes, 1 for each category)

        self.model.fit(np.array(train_data), train_cat, epochs=5, batch_size=16)                     #Train the machine learning model with the training data for 5 epochs, can change epochs and batch_size for different performance

        accuracy = self.model.evaluate(np.array(test_data), test_cat, batch_size=16)                 #Test the machine learning model with the test data and report accuracy, can change batch size, seems best as same value as training

        print("Final test accuracy: " + str(accuracy[1]))                                       #Print the final accuracy of the test data
        print("--------")

        """
        Finally wait for the user to enter a sentence and classify this as well.
        """

    def to_input(self, sentence):                                                         #Function to change a string of words to an array of numbers in the vocabulary
        data = []
        for i, word in enumerate(sentence.split()):                                 #For each word in this line, and count the lines with i
            if i >= self.utt_length:                                                     #Go up to utt_length - 1, because the first word is not a label in the sentence
                break														        #Stop reading this line when encountered a 11th utterence 
            if word in self.vocabulary:
                data.append(self.vocabulary[word])                                       #If the word is in the vocabulary, add this value to the data
            else:
                data.append(0)                                                      #If the word we are testing is not in the vocabulary, add a 0
        for j in range(i,self.utt_length - 1):												
            data.append(1)								                            #Continue to write "1"s as padding up to 10 utterences
        return(data)

    def get_classifier(self, sentence):			                        #Wait for input of the user
        input_data = self.to_input(sentence)                                                    #Encode given sentence
        cat = self.model.predict(np.array([input_data,]))                                #Predict with this sentence as input
        for key, value in self.vocabulary.items():
            if value == np.argmax(cat) + 2:                                         #Get the value corresponding to this integer (+2, since mapping started at 2)
                return(key)





