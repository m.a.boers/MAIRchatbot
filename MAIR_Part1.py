#First open all folders, then read through all the subfolders to see if there are json files in there.
#Read all folders in the data folder and then in all subfolders read the json files.
import os, json

mainpath = "data\\"
subfolders = os.listdir(mainpath)                                       #Every of these folders contains a dataset.
for folder in subfolders:
    jsonfolders = os.listdir(mainpath + folder)
    for jsonfolder in jsonfolders:                                      #Every of these folders contains a conversation.
        path = mainpath + folder + "\\" + jsonfolder + "\\"             #Get the path to the current folder.
        
        labelfile = open(path + "label.json").read()                    #First load the label json file.
        labeljson = json.loads(labelfile)
        
        logfile = open(path + "log.json").read()                        #Then load the log json file.
        logjson = json.loads(logfile)
        
        textfile = open(path + "%s.txt" % labeljson["session-id"],"w")	#Create a textfile with session-id as its name to write output in.
        
        print("session-id: " + labeljson["session-id"])                 #Output the session-id to console and textfile.
        print("session-id: " + labeljson["session-id"], file=textfile)
        
        print(labeljson["task-information"]["goal"]["text"])            #Output the task to console and textfile.
        print(labeljson["task-information"]["goal"]["text"], file=textfile)
        
        for turn in logjson["turns"]:                                   #Go through all turns form the system.
            index = turn["turn-index"]                                  #Get the turn index.
            print("system: " + turn["output"]["transcript"])            #Print the system transcript for this turn.
            print("system: " + turn["output"]["transcript"], file=textfile)
            for userturn in labeljson["turns"]:
                if userturn["turn-index"] == index:                     #Find the user turn with the same index.
                    print("user: " + userturn["transcription"])         #Print the user transcript for corresponding turn.
                    print("user: " + userturn["transcription"], file=textfile)
                    
        textfile.close()		                                        #Close text file.
        
        print("---------")
        input("Press enter to read the next dialog")                    #Wait for user input to go to the next dialog.
        print("---------")

