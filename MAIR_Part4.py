from RestaurantCheck import MakeList, restaurantCheck, valCheck, Restaurant #!m!
from MAIR_Part2 import Classifier
from MAIR_Part3 import Parser
import sys

# Constants for system-utterences
WELCOME   = 'Hello, welcome to the UU MAIR restaurant system! You can ask for restaurants by area, price range or food type. How may I help you?\n'
FOOD      = 'Which food type?\n'                                               
PRICE     = 'Which price-range?\n'                                            
AREA      = 'In which area?\n'                                                   
THANKYOU  = 'Thank you for using our service, press enter to restart.\n'  
TRY_AGAIN = "We can't help you with that, Please try again.\n"           
HELLO     = 'Hello to you too! How may I help you?\n'                         

#Speechact class, to return speech act and possible keywords in a structured way
class SpeechAct:
    def __init__(self, act, food = None, price = None, area = None, phone = None, addr = None, postcode = None):
        self.act = act
        self.food = food
        self.price = price
        self.area = area
        self.phone = phone
        self.addr = addr
        self.postcode = postcode
    def __str__(self): 
        return "%s, %s, %s, %s, %s, %s, %s" %(self.act, self.food, self.price, self.area, self.phone, self.addr, self.postcode)

#Demand class, to keep track of the current demand for a restaurant.
class Demand:
    def __init__(self):
        self.food = ""
        self.area = ""
        self.price = ""
        self.order = [] #List with strings in the order of getting the values (food, area and price)
    def update(self, speechact):
        if self.food == "" and not speechact.food == "":
            self.food = speechact.food
            self.order.append("food")
        if self.area == "" and not speechact.area == "":
            self.area = speechact.area
            self.order.append("area")
        if self.price == "" and not speechact.price == "":
            self.price = speechact.price
            self.order.append("price")
            
    def get_order(self):
        result = []
        for var in self.order:
            if var == "food":
                result.append(0)
            if var == "price":
                result.append(2)
            if var == "area":
                result.append(1)
        return result
        
'''
Utters a given "system_uterence", waits for input, classifies the input to a speech act and possible keywords and their values and returns the speech_act, keywords and values.
'''
def getInput(system_utterence):
    act = ""
    area = ""
    food = ""
    price = ""
    phone = False
    addr = False
    postcode = False
                  
    user_utterence = input(system_utterence)     #print system_uterence and drop input in user_utterence
    
    #If nothing was entered, simply return null.
    if user_utterence == "":
        return SpeechAct('null')
    
    user_utterence = user_utterence.replace("'", "")        # remove "'" (replace by "")
    user_utterence = user_utterence.replace(",", "")        # remove "," (replace by "")
    user_utterence = user_utterence.replace(".", "")        # remove "." (replace by "")
    user_utterence = user_utterence.replace("?", "")        # remove "?" (replace by "")
    user_utterence = user_utterence.replace("!", "")        # remove "!" (replace by "")       
    user_utterence = user_utterence.lower()                 # lower case only
    
    #First get the classifier for this statement.
    act = classifier.get_classifier(user_utterence)
    
    #Then if the act is inform, get the values
    if act == 'inform':
        values = parser.get_values(user_utterence)
        food = values[0]
        area = values[1]
        price = values[2]
    
    #Now put the values in a SpeechAct to return as awnser and fill if this is a request. 
    answer = SpeechAct(act, food, price, area, phone, addr, postcode)
    if answer.act == 'request':                                     
        if 'post' in user_utterence or 'code' in user_utterence:                            
            answer.postcode = True  
        if 'address' in user_utterence or 'adress' in user_utterence or 'addres' in user_utterence:                             
            answer.addr = True  
        if 'phone' in user_utterence or 'number' in user_utterence:                              
            answer.phone = True                                     
    return answer                                                   	

#Function to give a restaurant suggestion with values in given order.	
def restSuggest(restaurant, order, food, area, price):
    food_line = f"serves {food} food"
    area_line = f"is in the {area} part of town"
    price_line = f"is in the {price} price range"
    list = [food_line, area_line, price_line]											## The 'order' list should contain a 0, 1, 2 for food, area and price resp. in the order they were requested
    if len(order) == 1:																	## Seems basic but is much more functional than a loop 
        final_line = f"{restaurant} {list[order[0]]}.\n"
    elif len(order) == 2:
        final_line = f"{restaurant} {list[order[0]]} and {list[order[1]]}.\n"
    else:
        final_line = f"{restaurant} {list[order[0]]}, {list[order[1]]} and {list[order[2]]}.\n"
    return final_line
            
class StateMachine:
    #Initialize the StateMachine, first state is always Welcome state. Keeps track of demand and user input.
    def __init__(self):
        self.states        = {}
        initial            = WelcomeState()
        self.addState("welcome", initial)
        self.currentState  = initial
        self.demand        = Demand()
        self.curInput      = None
        self.reqMode       = False
        self.restList      = []
        self.restIndex     = 0
    
    #Set the statemachine in the state to this sm and then save the state. 
    #(Name is the same as corresponding act for convenience)
    def addState(self, name, state):
        state.setStateMachine(self)
        self.states[name] = state
        
    def next(self, stateName):
        #Check if name of the state can be found in the list, otherwise try again.
        self.previousState = self.currentState
        if stateName in self.states:
            self.currentState = self.states[stateName]
        else:
            self.currentState = self.states["tryagain"]
        
        #Run the current state, if there is any unknown error, restart the state machine.
        try:
            self.currentState.run()
        except:
            self.next("tryagain")
        
class State:
    #Keep a reference to the state machine in states, to use it for data and switching between states.
    def setStateMachine(self, stateMachine):
        self.sm = stateMachine
        
    def run(self):
        #Needs to be implemented to run something.
        print("Run for this state is not implemented!")
    
    def next(self):
        #Needs to be implemented to go to the next state.
        print("Next for this state is not implemented!")

#State to say Welcome and move to next.        
class WelcomeState(State):
    #Say welcome, get and save input.
    def run(self):
        self.sm.curInput = getInput(WELCOME)
        self.next()
        
    def next(self):
        self.sm.next(self.sm.curInput.act)

#State to say Hello and move to next.        
class HelloState(State):
    #Say hello, get and save input.
    def run(self):
        self.sm.curInput = getInput(HELLO)
        self.next()
        
    def next(self):
        self.sm.next(self.sm.curInput.act)

#State to process information given.
class InformState(State):
        
    #Then in run parse the input, put it in demand.
    def run(self):
        self.sm.demand.update(self.sm.curInput)
        self.next()
    
    def next(self):
        #If we are already in a request state, move to try again.
        if self.sm.reqMode:
            self.sm.next("tryagain")
            
        sm = self.sm
        demand = sm.demand
        rest_list = restaurantCheck(total_rest_list, demand.food, demand.price, demand.area)
        
        #No restaurants remaining.
        if len(rest_list) < 1:
            sm.next("norest")
            
        #One restaurant remaining shortcut.
        elif len(rest_list) == 1:
            sm.restList = rest_list
            sm.next("suggestion")
        
        #If there are still multiple, get more information if we don't have it yet.
        else:
            if demand.food == "":                                    
                sm.next("food")
            elif demand.price == "":                                    
                sm.next("price")
            elif demand.area == "":                                     
                sm.next("area")
            
            #We have all the variables now and there is apparently more then 1 restaurant remaining. Go to suggestions.
            else:
                sm.restList = rest_list
                sm.next("suggestion")

#State that lets the user know that there are no restaurants.                
class NoRestState(State):
    #Print the demands in order of requests.
    def run(self):
        demand = self.sm.demand
        food_line = f"serves {demand.food} food"
        area_line = f"is in the {demand.area} part of town"
        price_line = f"is in the {demand.price} price range"
        order = demand.get_order()
        list = [food_line, area_line, price_line]											## The 'order' list should contain a 0, 1, 2 for food, area and price resp. in the order they were requested

        if len(order) == 1:																	## Seems basic but is much more functional than a loop 
            final_line = f"There is no restaurant that {list[order[0]]}.\n"
        elif len(order) == 2:
            final_line = f"There is no restaurant that {list[order[0]]} and {list[order[1]]}.\n"
        else:
            final_line = f"There is no restaurant that {list[order[0]]}, {list[order[1]]} and {list[order[2]]}.\n"
            
        print(final_line)
        
        self.next()
    
    #After stating that there is no restaurant, say goodbye and start over.
    def next(self):
        self.sm.next("bye")
        
#State that can give a suggestion for a restaurant.
class SuggestionState(State):

    def run(self):
        sm = self.sm
        sm.reqMode = True
        demand = sm.demand
        sm.curInput = getInput(restSuggest(sm.restList[sm.restIndex].restaurant, demand.get_order(), sm.restList[sm.restIndex].food, 
                        sm.restList[sm.restIndex].area, sm.restList[sm.restIndex].price))
        self.next()
        
    def next(self):
        if self.sm.curInput.act == ('deny' or 'negate' or 'null' or 'reqalts' or 'reqmore'):
            self.sm.next("reqalts")
        else:
            self.sm.next(self.sm.curInput.act)

class RequestState(State):
    def run(self):
        if not self.sm.reqMode:
            self.sm.curInput = getInput(TRY_AGAIN)
        else:
            suggestion_input = self.sm.curInput
            rest_list = self.sm.restList
            i = self.sm.restIndex
            templist = []
            
            #Check what kind of requests have been made.
            if suggestion_input.phone:                     					
                templist.append(f"phone number is {rest_list[i].phone}")      
            if suggestion_input.addr:                     					
                templist.append(f"address is {rest_list[i].addr}")             
            if suggestion_input.postcode:                   				
                templist.append(f"postcode is {rest_list[i].postcode}")           
            
            #Build the reply string according to the amount of requested data.
            if len(templist) == 1:											
                self.sm.curInput = getInput(f"{rest_list[i].restaurant}'s {templist[0]}.\n")
            elif len(templist) == 2:
                self.sm.curInput = getInput(f"{rest_list[i].restaurant}'s {templist[0]} and the {templist[1]}.\n")
            elif len(templist) == 3:
                self.sm.curInput = getInput(f"{rest_list[i].restaurant}'s {templist[0]}, the {templist[1]} and {templist[2]}.\n")
        self.next()
        
    def next(self):
        self.sm.next(self.sm.curInput.act)

class RequestAltState(State):
    def run(self):
        if not self.sm.reqMode:
            self.sm.curInput = getInput(TRY_AGAIN)
            self.sm.next(self.sm.curInput.act)
        else:
            self.sm.restIndex += 1
            self.next()
    def next(self):
        if self.sm.restIndex >= len(self.sm.restList):
            self.sm.next("noalts")
        else:
            self.sm.next("suggestion")
            
            
class NoAltsState(State):
    #Print the demands in order of requests.
    def run(self):
        demand = self.sm.demand
        food_line = f"serve {demand.food} food"
        area_line = f"are in the {demand.area} part of town"
        price_line = f"are in the {demand.price} price range"
        order = demand.get_order()
        list = [food_line, area_line, price_line]											

        if len(order) == 1:																	
            final_line = f"There are no more alternative restaurants that {list[order[0]]}.\n"
        elif len(order) == 2:
            final_line = f"There are no more alternative restaurants that {list[order[0]]} and {list[order[1]]}.\n"
        else:
            final_line = f"There are no more alternative restaurants that {list[order[0]]}, {list[order[1]]} and {list[order[2]]}.\n"
            
        print(final_line)
        
        self.next()
    
    #After stating that there is no restaurant, say goodbye and start over.
    def next(self):
        self.sm.next("bye")
        
            
#State to get information from the user.
class GetInfoState(State):
    def getInfo(self, text):
        self.sm.curInput = getInput(text)
        self.next()
    def next(self):
        self.sm.next(self.sm.curInput.act)
        
class FoodState(GetInfoState):
    def run(self):
        self.getInfo(FOOD)
        
class PriceState(GetInfoState):
    def run(self):
        self.getInfo(PRICE)
        
class AreaState(GetInfoState):
    def run(self):
        self.getInfo(AREA)
                
#State to repeat the previous state.
class RepeatState(State):
    #No need to do anything here, just repeat the last state.
    def run(self):
        self.next()
    
    #Go back to the prev state to repeat it.
    def next(self):
        self.sm.currentState = self.sm.previousState
        self.sm.currentState.run()
        
#State to get input again.
class TryAgainState(State):
    
    def run(self):
        self.sm.curInput = getInput(TRY_AGAIN)
        self.next()
     
    def next(self):
        self.sm.next(self.sm.curInput.act)

#State to restart the machine.        
class RestartState(State):
    #Reset variables in the state machine.
    def run(self):
        print("--------------")
        self.sm.demand    = Demand()
        self.sm.input     = None
        self.sm.reqMode   = False
        self.sm.restList  = []
        self.sm.restIndex = 0
        self.next()
        
    #Then go to the welcome state.
    def next(self):
        self.sm.next("welcome")
        
#State to say Thanks and restart.
class ExitState(State):
    #Says thankyou and waits for user input before restarting.
    def run(self):
        input(THANKYOU)
        self.next()
        
    def next(self):
        self.sm.next("restart")

"""
The actual code to run the state machine is here.

"""

#Initialize the other parts that are required.        
classifier      = Classifier()
parser          = Parser()
total_rest_list = MakeList()

#Then make the state machine, initialize the states and start the state machine.   
sm = StateMachine()

sm.addState("hello",      HelloState())
sm.addState("inform",     InformState())
sm.addState("norest",     NoRestState())
sm.addState("suggestion", SuggestionState())
sm.addState("food",       FoodState())
sm.addState("price",      PriceState())
sm.addState("area",       AreaState())
sm.addState("bye",        ExitState())
sm.addState("thankyou",   ExitState())
sm.addState("request",    RequestState())
sm.addState("reqalts",    RequestAltState())
sm.addState("noalts",     NoAltsState())
sm.addState("repeat",     RepeatState())
sm.addState("tryagain",   TryAgainState())
sm.addState("restart",    RestartState())

sm.next("welcome")
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        