import csv

class Restaurant:
    def __init__(self, restaurant, price, area, food, phone, addr, postcode):
        self.restaurant = restaurant
        self.price = price
        self.area = area
        self.food = food
        self.phone = phone
        self.addr = addr
        self.postcode = postcode
    def __str__(self):
        return "%s, %s, %s, %s, %s, %s, %s" %(self.restaurant, self.price, self.area, self.food, self.phone, self.addr, self.postcode)


def MakeList():
    rest_list = []
    with open('restaurantinfo.csv') as csvfile:
        reader = csv.reader(csvfile)
        for x in reader:
            rest = Restaurant(x[0], x[1], x[2], x[3], x[4], x[5], x[6])
            rest_list.append(rest)
    rest_list.pop(0)                        # remove the head, because it contains only the variable names
    return rest_list


# default NONE, lijst is object van alle info, boolean terug; gebruik boolean om lijst buitenaf aan te passen (elementen weg te halen)
# special case for if(any): same as when value is NONE
def restaurantCheck(restaurants, food_val, price_val, area_val):
    new_list = []
    for x in restaurants:
        if valCheck(x, food_val, price_val, area_val):      # returns true when a match is found, so the restaurant is appended to list
            new_list.append(x)
    return new_list

def valCheck(restaurant, food_val, price_val, area_val):
    match = True          # if match becomes false, then the restaurant does not satisfy the demands
    if (food_val is not "") and (restaurant.food != food_val):
        if food_val != "any food":
            match = False
            return match
    if (price_val is not "") and (restaurant.price != price_val):
        if price_val != "any price":
            match = False
            return match
    if (area_val is not "") and (restaurant.area != area_val):
        if area_val != "any area":
            match = False
            return match
    return match