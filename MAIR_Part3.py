import sys, copy, os, json

class Node:
    #type is a list of types, since a node can have multiple types!!!
    def __init__(self, word, types, left=None, right=None):
        self.word = word
        self.types = types
        self.left = left
        self.right = right
        self.flags = []
    def __str__(self):
        if self.left == None:
            if self.right == None:
                return "Node: " + self.word + ", " + str(self.types)
            return "Node: " + self.word + ", " + str(self.types) + ",\n (Left: " + str(self.left) + ")"
        if self.right == None:
            "Node: " + self.word + ", " + str(self.types) + ",\n (Right: " + str(self.right) + ")"
        return "Node: " + self.word + ", " + str(self.types) + ",\n (Left: " + str(self.left) + ",\n Right: " + str(self.right) + ")"
        
    def keyCheck(self, index, key, flag):
        #Checks whether a key is in this node and most importantly checks the children of this node.
        #print(str(index) + " = " + self.word)
        if self.left is None and self.right is None: # This is a leaf! Set a flag and return!
            if flag not in self.flags:
                self.flags.append(flag)
            return
        
        if not self.left is None and key in self.left.word:
            self.left.keyCheck(index + 1, key, flag)
            if self.right is None:
                if flag not in self.flags:
                    self.flags.append(flag)
                return
            else:
                if flag in self.right.flags:
                    if flag not in self.flags:
                        self.flags.append(flag)
                    return
        if not self.right is None and key in self.right.word:
            self.right.keyCheck(index + 1, key, flag)
            if self.left is None:
                if flag not in self.flags:
                    self.flags.append(flag)
                return
            else:
                if flag in self.left.flags:
                    if flag not in self.flags:
                        self.flags.append(flag)
                    return
    
    #Traceback to get all the nodes for the given flag
    def getFlagNodes(self, flag):
        if not self.left is None:
            self.left.getFlagNodes(flag)
        if not self.right is None:
            self.right.getFlagNodes(flag)
        #if flag in self.flags:
            #print(self.word)
    
    #Traceback to check if given flag is disjunct or not
    def checkDisjunct(self, flag): #Disjunct if there are no nodes with this flag and another flag combined.
        if flag in self.flags and len(self.flags) > 1:
            return False
        if not self.left is None:
            if not self.left.checkDisjunct(flag):
                return False
        if not self.right is None:
            if not self.right.checkDisjunct(flag):
                return False
        return True

class Parser:
    # rules
    # rule 1: Y, X\Y --> X
    # rule Y\Z, X\Y --> X\Z also a rule right???
    # rule 2: X/Y, Y --> X
    # rule 3: X/Y, Y/Z --> X/Z
    def Eliminate(self, nl, nr):
        #print("Try to eliminate "+l+" and "+r)
        """
        if ('(' in l) or ('(' in r):
            # implement selectBracketRule(l,r)
            # For example: use newstr = oldstr.replace(')/', '')
            return 0
        else:"""
        l = nl.types[0]
        r = nr.types[0]
        #Check for ((np\s)/(np\s))
        if r == "(np\\s)/(np\\s)":
            #Ignore
            return 0
        if l == "(np\\s)/(np\\s)":
            if r == "np\\s":
                list = []
                list.append("np\\s")
                return Node(nl.word + " " + nr.word, list, nl, nr)
        # check for the right rules
        if '/' in l:
            #print("/")
            new_type = self.rightRule(nl, nr)
            return new_type
        # check for the left rule
        if '\\' in r:
            #print("\\")
            if '\\' in l:
                new_type = self.bothRule(nl, nr)
                return new_type
            else:
                new_type = self.leftRule(nl, nr)
                return new_type
        else:
            return 0

    # rule 1: X, X\Y --> Y 
    """(previously Y, X\Y --> X )"""
    def leftRule(self, nl, nr):
        l = nl.types[0]
        r = nr.types[0]
        parts = r.split('\\')
        if parts[0] == l:
            parts[1] = parts[1].replace("(","")
            parts[1] = parts[1].replace(")","")
            list = []
            list.append(parts[1])
            return Node(nl.word + " " + nr.word, list, nl, nr)
        else:
            return 0

    # rule 2: X/Y, Y --> X
    def rightRule(self, nl, nr):
        l = nl.types[0]
        r = nr.types[0]
        parts = l.split('/')
        if parts[1] == r:
            parts[0] = parts[0].replace("(","")
            parts[0] = parts[0].replace(")","")
            list = []
            list.append(parts[0])
            return Node(nl.word + " " + nr.word, list, nl, nr)
        else:
            return 0

    # rule 3: X\Y, Y\Z --> X\Z
    """X/Y, Y/Z --> X/Z"""
    def bothRule(self, nl, nr):
        l = nl.types[0]
        r = nr.types[0]
        l_parts = l.split('\\')
        r_parts = r.split('\\')
        if l_parts[1] == r_parts[0]:
            list = []
            list.append(l_parts[0] +'\\'+ r_parts[1])
            return Node(nl.word + " " + nr.word, list, nl, nr)
        else:
            return 0

    def __init__(self):
        #Set up the dictionary.
        self.dictionary = {}
        typeDict = {}

        #Load the dictionary file (give an error if it does not exist!)
        if not os.path.isfile("vocabularyfull.txt"):
            print("Vocabulary file vocabularyfull.txt was not found!") 
            sys.exit()
            
        if not os.path.isfile("ontology_dstc2.json"):
            print("Keyword file ontology_dstc2.json was not found!") 
            sys.exit()
            
        grammar_list2 = []

        with open("vocabularyfull.txt","r") as vocabfile:
            for line in vocabfile:
                parts = line.split()
                if parts[0] == "#":
                    continue #Ignore comments
                if parts[0] == "grammar_list": #Get the grammar_list
                    grammar_list2 = parts[1].split(',')
                    continue
                if parts[0] == "type_list":
                    for i, typ in enumerate(parts[1].split(',')):
                        typeDict[grammar_list2[i]] = typ
                    continue
                #Then add the current type for all word following it:
                for i, word in enumerate(parts[1].split(',')):
                    if word not in self.dictionary:
                        self.dictionary[word] = []
                    self.dictionary[word].append(typeDict[parts[0]]) #Directly add the correct type to the dictionary.
         
        """
        Finally an algorithm to find the subtrees to values for all variables.

        CAUTION need to have the ontology_dstc2.json in the same folder for the algorithm to work!
        """

        
            
        #Load the json file with the keywords:
        keyfile = open("ontology_dstc2.json").read()
        keyjson = json.loads(keyfile)

        self.valfood  = keyjson["informable"]["food"]
        self.valarea  = keyjson["informable"]["area"]
        self.valprice = keyjson["informable"]["pricerange"]

        #Add some values to areas, since those are extra values compared to price and food, which is more specific.
        self.valarea.append("center") #misspelled centre
        self.valarea.append("any area")
        self.valprice.append("any price")
        self.valfood.append("any food")

        #These are the keyword variable related words that we are also looking for.
        #These are ignored ON PURPOSE if there is no value found with the json values, since otherwise for example restaurant would always be positive for pricerange and food.
        self.keyfood  = ["restaurant","restaurants","place","type","sells","serve","served","serves","serving"]
        self.keyarea  = ["part","parts","town","beside","central","city","downtown","anywhere","inner","park","side"]
        self.keyprice = ["restaurant","class","fancy","medium","priced","prices","range","reasonably"]

    """
    Load the user input sentence.
    """        
    def get_values(self, sentence):
        sentence = sentence.replace("'", "")        # remove "'" (replace by "")
        sentence = sentence.replace(",", "")        # remove "," (replace by "")
        sentence = sentence.replace(".", "")        # remove "." (replace by "")
        sentence = sentence.replace("?", "")        # remove "?" (replace by "")
        sentence = sentence.replace("!", "")        # remove "!" (replace by "")
        sentence = sentence.lower()                 # lower case only
        sentence = sentence.split()                 # split on each " "

        #translate sentence to types
        nodes = []
        for word in sentence:                       #for every word in the sentence
            if word not in self.dictionary:
                #Ignore this word, since we can't parse it.
                continue 
            nodes.append(Node(word, self.dictionary[word]))                 #Append the found type to the "type"-sentence

        """
        Split words with multiple types into multiple sentences.
        """

        #First make sure that all nodes with multiple types are split, the algorithm will make sure all sentences are checked correctly after this.
        stack = []
        nodeStack = []
        nodeStack.append(nodes)
        while len(nodeStack) > 0:
            cur = nodeStack.pop()
            for i, node in enumerate(cur):
                if i == len(cur) - 1:
                    if len(node.types) == 1: #Last node found, only has one type, done, no need to split further, add it to the actual stack.
                        stack.append(cur)
                        break
                if (len(node.types)) > 1: #Replace the cur with new node lists on the stack for all types in the current node.
                    for type in node.types:
                        newCur = copy.deepcopy(cur)
                        newCur[i] = Node(node.word, [])
                        newCur[i].types.append(type)
                        nodeStack.append(copy.deepcopy(newCur))
                    break
                    
                        
        """
        Find the first implementation tree of the sentence using DFS.
        """
                                    
        discovered = []

        finalTree = []
        
        #We use the max length to determine whether a better parse tree has been found.
        maxLength = 999 

        #First build up the tree with a simple depth first search algorithm.
        while len(stack) > 0:
            cur = stack.pop()
            discovered.append(cur) #This set of nodes has been seen now.
            if len(cur) == 1:
                #This is done!!! This is the only remaining type!
                finalTree = cur
                maxLength = 1;
                break;
            if len(cur) < maxLength:
                maxLength = len(cur)
                finalTree = copy.deepcopy(cur)
            for i in range(len(cur)): #Go through all nodes to see if we can combine any.
                if i+1 >= len(cur):
                    break; #Done here.
                newNode = self.Eliminate(cur[i], cur[i+1])
                if newNode:
                    #Do something. Have to add this as a new posibility to the stack, if it is a new option.
                    copyCur = copy.deepcopy(cur)
                    del copyCur[i]
                    copyCur[i] = newNode
                    if copyCur not in discovered:
                        stack.append(copyCur)

        food  = ""
        area  = ""
        price = ""
        pathIndex = 0
        disjunctString = "Disjunct: "

        #Check for values in all current nodes in the tree, if the length was 1 (type = s), this will only happen once
        for curNode in finalTree:
            for key in self.valfood:
                if key in curNode.word: #Match found, look further in this node.
                    if food is "":
                        food = key
                    pathIndex += 1
                    curNode.keyCheck(0, key, "food")

            #Check extra keywords if a food was found.
            if not food is "":        
                for key in self.keyfood:
                    if key in curNode.word: #Match found, look further in this node.
                        pathIndex += 1
                        curNode.keyCheck(0, key, "food")
                        
            if not food is "":
                curNode.getFlagNodes("food")        
                        
            pathIndex = 0
                
            for key in self.valarea:
                if key in curNode.word: #Match found, look further in this node.
                    if area is "":
                        area = key
                    pathIndex += 1
                    curNode.keyCheck(0, key, "area")

            #Check extra keywords if a area was found.
            if not area is "":        
                for key in self.keyarea:
                    if key in curNode.word: #Match found, look further in this node.
                        pathIndex += 1
                        curNode.keyCheck(0, key, "area")

            pathIndex = 0

            if not area is "":
                curNode.getFlagNodes("area")  

            for key in self.valprice:
                if key in curNode.word: #Match found, look further in this node.
                    if price is "":
                        price = key
                    pathIndex += 1
                    curNode.keyCheck(0, key, "price")

            #Check extra keywords if a price was found.
            #No need to check for moderately, this happens to contain moderate, so this is parsed correctly already.
            if not price is "":        
                for key in self.keyprice:
                    if key in curNode.word: #Match found, look further in this node.
                        pathIndex += 1
                        curNode.keyCheck(0, key, "price")

            if not price is "":            
                curNode.getFlagNodes("price")

            
            if not food is "":
                if curNode.checkDisjunct("food"):
                    disjunctString += "food "
            if not area is "":
                if curNode.checkDisjunct("area"):
                    disjunctString += "area "
            if not price is "":    
                if curNode.checkDisjunct("price"):
                    disjunctString += "price "
        
        result = [food, area, price]
        
        return(result)
                    
    

    









