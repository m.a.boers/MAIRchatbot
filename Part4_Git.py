Opmerking voor Martin: de index/array die volgorde moet bijhouden kan niet in de/een while loop omdat er mogelijk al dingen opgeslagen zijn. Beter bij parsen. 
						In line 44 en 53 wordt nu naar order gerefereerd, maar deze variabele bestaat nu nog niet. 
						Ik weet nu niet waar ik deze op moet bouwen; of zie iig niet hoe ik dit in Marijn zn deel kwijt kan. Even kijken of jij het bij samenvoegen wel kwijt kan 
						(Maybe in getInput??), anders pak ik t zo weer op.

order = [2, 0]																			## the list should be extended every time a frame is updated, with a value of 0, 1, 2 for food, area and price resp. 
																						## See comments (top) on why I can't implement it\
	
def noRest(order, food, area, price):
    food_line = f"serves {food} food"
    area_line = f"is in the {area} part of town"
    price_line = f"is in the {price} price range"
    list = [food_line, area_line, price_line]											## The 'order' list should contain a 0, 1, 2 for food, area and price resp. in the order they were requested
    if len(order) == 1:																	## Seems basic but is much more functional than a loop 
        final_line = f"There is no restaurant that {list[order[0]]}."
    if len(order) == 2:
        final_line = f"There is no restaurant that {list[order[0]]} and {list[order[1]]}."
    else:
        final_line = f"There is no restaurant that {list[order[0]]}, {list[order[1]]} and {list[order[2]]}."
    print(final_line)
	
	
def restSuggest(restaurant, order, food, area, price):
    food_line = f"serves {food} food"
    area_line = f"is in the {area} part of town"
    price_line = f"is in the {price} price range"
    list = [food_line, area_line, price_line]											## The 'order' list should contain a 0, 1, 2 for food, area and price resp. in the order they were requested
    if len(order) == 1:																	## Seems basic but is much more functional than a loop 
        final_line = f"{restaurant} {list[order[0]]}."
    if len(order) == 2:
        final_line = f"{restaurant} {list[order[0]]} and {list[order[1]]}."
    else:
        final_line = f"{restaurant} {list[order[0]]}, {list[order[1]]} and {list[order[2]]}."
    print(final_line)

	
	
	
    if len(rest_list) < 1:                               						## Dit was len(valCheck) bij Marijn, maar moet rest_list zijn (als dat idd de lijst met rests wordt die aan de voorwaarden voldoen)          
        noRest(order, demand.food, demand.area, demand.price)                   ## Replace 'order' with the list containing the order.           
        continue                                            #!n!Utter that there are no restaurants available and start over
    else:                                               #When there is 1 or more restaurant valid regarding demand
#!m!RestaurantCheck REFERENCE of restaurantCheck(rest_list, demand.food, demand.price, demand.area)	
        rest_list = restaurantCheck                     #!m!Create list of valid restuarants regarding demand
        i = 0                                           #alternative counter
        alts_required = True                            #Set alts_required (default setting)
        while alts_required and i < len(rest_list):      						## Dit was len(valCheck) bij Marijn, maar moet rest_list zijn (als dat idd de lijst met rests wordt die aan de voorwaarden voldoen)
            alts_required = False                           					#Clear alts_required as initial setting within while loop, untill set when alternatives are required
            suggestion_input = getInput(restSuggest(rest_list[i][0], order, demand.food, demand.area, demand.price))  ## Replace 'order'  with the list containing the order
            if suggestion_input.act == ('deny' or 'negate' or 'null' or 'reqalts' or 'reqmore'):    #If reply is any of these speech acts consider that the user requires an alternative
                alts_required = True                                                                        
                continue                                                        #Set alts_required and start this loop over at the top
            elif suggestion_input.act == 'request':         					#Else if reply is a request
    		    templist = []
                if suggestion_input.phone:                     					#If reply seems to ask for phone number
                    extend.templist(f"phone number is {rest_list[i][4]}")       #Add phone number line to list
                if suggestion_input.addr:                     					#If reply seems to ask for  address
                    extend.templist(f"adress is {rest_list[i][5]}")             #Add adress line to list
                if suggestion_input.postcode:                   				#If reply seems to ask for postcode
                    extend.templist(f"postcode is {rest_list[i][6]}")           #Add postcode to list
                if len(templist) == 1:											#Print output depending on amount of information requested
                    print(f"{rest_list[i][0]}'s {templist[0]}.")
                if len(templist) == 2:
                    print(f"{rest_list[i][0]}'s {templist[0]} and the {templist[1]}.")
                if len(templist) == 3:
                    print(f"{rest_list[i][0]}'s {templist[0]}, the {templist[1]} and {templist[2]}.")
                if suggestion_input.phone or suggestion_input.addr or suggestion_input.postcode:    #If reply did ask for any restaurant details
                    input(THANKYOU)                                 
                    sys.exit()                                      #!n!Thank the user and exit dialog upon key press
            i += 1                                          #Increase the alternative counter with 1

			

			