# input sentence string
# step 1: split string on white spaces, make it lower case and safe it in a list
s = "Some strings attached"
s = s.lower()
wordlist = s.split( )

# step 2: look up each word of the list in the dictionary
dict = {'some': 'first', 'strings': 'three', 'attached': 'words'}
type_of_word = []
for i in range(len(wordlist)):
    type_of_word.append(dict[wordlist[i]])


# step 3: once we have the type for each word, run through the list and apply one of the rules
while len(type_of_word) != 1:
    for i in range(len(type_of_word)-1):
        l_word = type_of_word[i]
        r_word = type_of_word[i+1]
        # try to apply the rules and replace the types
    del type_of_word[2] # this is still useless, just here to stop the while loop
    del type_of_word[1]



# rules

# rule 1: Y, X\Y --> X
# rule Y\Z, X\Y --> X\Z also a rule right???
# rule 2: X/Y, Y --> X
# rule 3: X/Y, Y/Z --> X/Z
def selectRule(l, r):
    if '()' in l or '()' in r:
        # implement selectBracketRule(l,r)
        # For example: use newstr = oldstr.replace(')/', '')
        return
    else:
        # check for the right rules
        if '/' in l:
            if '/' in r:
                new_type = bothRule(l, r)
                return new_type
            else:
                new_type = rightRule(l, r)
                return new_type
        # check for the left rule
        if '\\' in r:
            new_type = leftRule(l, r)
            return new_type
        else:
            return

# rule 1: Y, X\Y --> X
def leftRule(l,r):
    parts = r.split('\\')
    if parts[1] == l:
        return parts[0]
    else:
        return

# rule 2: X/Y, Y --> X
def rightRule(l,r):
    parts = l.split('/')
    if parts[1] == r:
        return parts[0]
    else:
        return

# rule 3: X/Y, Y/Z --> X/Z
def bothRule(l,r):
    l_parts = l.split('/')
    r_parts = r.split('/')
    if l_parts[1] == r_parts[0]:
        return l_parts[0] + r_parts[1]
    else:
        return

# to test the rule
dict2 = {'chinese': 'np/n', 'restaurant': 'n', 'have': '(np\s)/(np\s)', 'and': '(s\s)/s', 'come': '(np\s)'}
#s2 = 'Chinese restaurant'
#s2 = 'have and'
s2 = 'have come'
s2 = s2.lower()
wordlist2 = s2.split()
types = []
for i in range(len(wordlist2)):
    types.append(dict2[wordlist2[i]])

left = types[0]
right = types[1]
print(selectRule(left, right))


# example to remove types from within a type
#type_of_word.strip('/np')

# split the type of words on the slashes
#type_of_word.split("\\")
#type_of_word.split("/")


# string formatting operator "My name is %s and weight is %d kg!" % ('Zara', 21)